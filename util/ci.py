# Copyright (c) 2019 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

import gitlab
import re
import requests
import yaml
from tracie_dashboard.settings import GITLAB_URL, GITLAB_TOKEN, MINIO_URL
from gitlab.exceptions import GitlabGetError

gl = None


def _connect_to_ci(project_path):
    global gl
    if gl is None:
        gl = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_TOKEN)
    return gl.projects.get(project_path)

class Job:
    __slots__ = ('id', 'project_path', 'pipeline_id', 'status', 'timestamp', 'device',
                 'trace_results')
    def __init__(self):
        self.id = None
        self.project_path = None
        self.pipeline_id = None
        self.status = None
        self.device = None
        self.timestamp = None
        self.trace_results = {}

    def url(self):
        return GITLAB_URL + "/" + self.project_path + "/-/jobs/" + str(self.id)

    def get_trace_results(self, job):
        if job.pipeline['status'] == 'running':
            return {}

        r = requests.get(MINIO_URL + "/artifacts/{}/{}/{}/traces/results.yml".format(self.project_path,
                                                                                     self.pipeline_id,
                                                                                     self.id))
        return yaml.safe_load(r.content) if r.status_code == 200 else {}

class Pipeline:
    __slots__ = ('id', 'project_path', 'status', 'mesa_repo', 'mesa_sha',
                 'test_job_ids')
    def __init__(self):
        self.id = None
        self.project_path = None
        self.status = None
        self.mesa_repo = None
        self.mesa_sha = None
        self.test_job_ids = []

    def url(self):
        return GITLAB_URL + "/" + self.project_path + "/pipelines/" + str(self.id)

def get_pipeline_ids(project_path, count=20):
    """Gets the latest 'count' pipeline ids"""

    project = _connect_to_ci(project_path)

    # Create a generator for the pipeline ids. python-gitlab fetches
    # results as needed in batches of 'per_page' (with 100 being the max).
    pipelines_gen = project.pipelines.list(as_list=False, per_page=min(100, count))

    return [p.id for _, p in zip(range(count), pipelines_gen)]

def get_pipeline(project_path, pid):
    project = _connect_to_ci(project_path)

    p = project.pipelines.get(pid)

    pipeline = Pipeline()
    pipeline.id = p.id
    pipeline.project_path = project_path
    pipeline.status = p.status
    pipeline.mesa_repo = "%s/%s" % (GITLAB_URL, project_path)
    pipeline.mesa_sha = p.sha

    for j in p.jobs.list(all=True):
        if "-traces" in j.name or "_traces" in j.name:
            pipeline.test_job_ids.append(j.id)

    return pipeline

def get_test_job(project_path, jid):
    project = _connect_to_ci(project_path)

    job = project.jobs.get(jid)

    test_job = Job()
    test_job.id = jid
    test_job.project_path = project_path
    test_job.pipeline_id = job.pipeline['id']
    test_job.status = job.status
    # TODO: Use a better way to figure out the device
    test_job.device = job.name.replace("-traces", "").replace("_traces", "").split(":")[0]
    test_job.trace_results = test_job.get_trace_results(job)
    test_job.timestamp = job.finished_at

    return test_job
