# Copyright (c) 2019 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

from django.shortcuts import get_object_or_404, render
from .cache import pipeline_result_for_id, job_result_for_id, all_device_results
import util.ci as ci
from util.image_diff import image_diff_png
import asyncio
from base64 import b64encode
import requests
from .models import TraceResult

async def all_pipeline_results(project_path):
    pids = ci.get_pipeline_ids(project_path)

    all_pipeline_results_coro = [pipeline_result_for_id(project_path, pid) for pid in pids]
    all_pipeline_results = await asyncio.gather(*all_pipeline_results_coro)

    return [pr for pr in all_pipeline_results if pr.status != "inprogress"]

def project(request, project_path):
    pipeline_results = asyncio.run(all_pipeline_results(project_path))
    context = {
        "pipeline_results": pipeline_results,
        "project_path": project_path
    }
    return render(request, "dashboard/project.html", context)

def job(request, project_path, jid):
    job_result = asyncio.run(job_result_for_id(project_path, jid))
    context = {
        "job_result": job_result,
        "project_path": project_path
    }
    return render(request, "dashboard/job.html", context)

def devices(request, project_path):
    pipeline_results = asyncio.run(all_pipeline_results(project_path))
    context = {
        "device_results": all_device_results(project_path, pipeline_results),
        "project_path": project_path
    }
    return render(request, "dashboard/devices.html", context)

def imagediff(request, project_path, jid, expected_sum, trace_name):
    trace = get_object_or_404(TraceResult, job_result=jid, expected_checksum=expected_sum, name=trace_name)

    context = {
        "trace": trace,
        "reference_png_url": trace.reference_png_url,
        "images_match": (trace.expected_checksum == trace.actual_checksum),
        "project_path": project_path
    }

    try:
        result = requests.get(trace.reference_png_url)
        result.raise_for_status()
        reference_png = result.content
    except:
        context["reference_failed"] = True
        context["diff_failed"] = True

    if  trace.expected_checksum != trace.actual_checksum:
        try:
            result = requests.get(trace.actual_png_url)
            result.raise_for_status()
            actual_png = result.content
            context['actual_png_url'] = trace.actual_png_url
        except:
            context["actual_failed"] = True
            context["diff_failed"] = True

        if not context["reference_failed"] and context["actual_failed"] :
            try:
                diff_png, red_pixel_count = image_diff_png(reference_png, actual_png)
                context["diff_png_b64"] = b64encode(diff_png).decode()
                context["red_pixel_count"] = red_pixel_count
            except:
                context["diff_failed"] = True

    return render(request, "dashboard/imagediff.html", context)
